import numpy as np

from scipy.stats import spearmanr

def genSentences(path, separator=" "):
    """
        Returns a generator over the splitted(over separator) sentences of the file passed as path.
    """

    fd = open(path, mode="r")

    line = fd.readline()

    while line != "":
        yield line.split(separator)
        line = fd.readline()

    fd.close()

def cosineSimilarity(a, b):
    """
        Returns the cosine similarity between the vectors a and b
    """
    a = np.asarray(a, dtype='float64')
    b = np.asarray(b, dtype='float64')

    dot = np.dot(a, b)

    norma = np.linalg.norm(a)
    normb = np.linalg.norm(b)

    return dot/(norma * normb)

def computeVocabulary(path):
    """
        In order to avoid parsing the embeddings.vec file multiple times while performing the word similarity task this function will build a dictionary
        with the lemmas as keys and a list of lists containing the sense embeddings of every known sense of that lemma as values.
    """

    fd = open(path, mode="r")

    # discards the first line
    fd.readline()
    line = fd.readline()

    voc = dict()

    while line != "":
        words = line.split()
        lemma = words[0][:words[0].rfind("_")]

        if lemma not in voc:
            voc[lemma] = list()

        voc[lemma].append(words[1:])

        line = fd.readline()

    fd.close()

    return voc

def wordSimilarityTask(wstPath, embeddingsPath):
    """
        Function used to test the trained embeddings on the word similarity task
    """

    generator = genSentences(wstPath, separator=",")
    # Discards the first sentence
    next(generator)

    gold = list()
    cosine = list()

    # this vocabulary maps from lemma to all its learnt sense embeddings
    voc = computeVocabulary(embeddingsPath)

    for words in generator:

        gold.append(words[2][:-1])

        if words[0] not in voc or words[1] not in voc:
            cosine.append(-1)
            continue

        s1 = voc[words[0]]
        s2 = voc[words[1]]

        score = -1

        for w1 in s1:
            for w2 in s2:
                score = max(score, cosineSimilarity(w1, w2))

        cosine.append(score)

    # evaluate the results and returns the accuracy
    return spearmanr(gold, cosine)[0]

if __name__ == "__main__":
    # Code used to evaluate the accuracy on the word similarity task of the trained embeddings

    combined_path = "resources/combined.csv"
    embeddings_path = "resources/embeddings.vec"

    accuracy = wordSimilarityTask(combined_path, embeddings_path)

    print("The obtained accuracy is: ", accuracy)
