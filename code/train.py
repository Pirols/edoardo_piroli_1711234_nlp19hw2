import os
import random

from gensim.models.callbacks import CallbackAny2Vec
from gensim.models import Word2Vec

from test import cosineSimilarity
from test import computeVocabulary
from test import wordSimilarityTask
from test import genSentences

class myCallBacks(CallbackAny2Vec):

    def __init__(self, embeddings_path, wst_path):
        self.epoch = 0
        self.wst_path = wst_path
        self.embeddings_path = embeddings_path
        self.acc_wst_history = list()

    def on_epoch_begin(self, model):
        if self.wst_path != "" and self.epoch > 0:
            self.acc_wst_history.append(wordSimilarityTask(self.wst_path, self.embeddings_path))
        self.epoch += 1

def getSenseEmbeddings(input_path, output_path):
    """ 
        Gets an embeddings.vec file as input and creates a new file as output_path with only sense embeddings
    """
    
    fd1 = open(input_path, mode="r")
    fd2 = open(output_path, mode="w")

    dim = int(fd1.readline().split()[1])

    line = fd1.readline()

    count = 0
    text = ""

    while line != "":

        words = line.split()

        # gathers only words containing _bn: hence only senses
        if "_bn:" in words[0] and len(words) == dim + 1:
            count += 1
            text += line
        
        line = fd1.readline()
    
    # adds this first line to make it in the Word2Vec format
    s = str(count) + " " + str(dim) + "\n" + text

    fd2.write(s)

    fd1.close()
    fd2.close()

def train(datasetPath, outputPath, wstPath="", temporaryPath="./test.vec", min_count=8, window=5, size=300, sample=6e-5, alpha=0.025, min_alpha=1e-4,
          epochs=12, sg=0, hs=1, cbow_mean=1, seed=None, callbacks=False):
    """
        This function performs the training of a CBOW Word2Vec model on the sentences contained in datasetPath. And saves the sense embeddings in the outputPath
    """

    if callbacks and wstPath == "":
        print("If callbacks is set to True you must also specify a word-similarity-task path! Returning")
        return -1

    # If seed is set, then the hash function and the weights' initialization won't change from run to run; otherwise they might
    if seed == None:
        seed = random.randint(0, 2**32)
    
    else:
        os.environ["PYTHONHASHSEED"] = "10"

    # creates the model and leaves it, purposefully, uninitialized - not feeding the sentences
    w2v_model = Word2Vec(min_count=min_count, window=window, size=size, sample=sample, alpha=alpha, min_alpha=min_alpha,
                         sg=sg, hs=hs, cbow_mean=cbow_mean, seed=seed, workers=1)

    # gets a generator of the sentences of the dataset to train on
    # and then builds the vocabulary needed to train
    generator = genSentences(datasetPath)
    w2v_model.build_vocab(sentences=generator)

    bs = myCallBacks(outputPath, wst_path=wstPath)

    for _ in range(epochs):
        # resets the generator since it won't contain anything at this point
        # and then trains on in for one epoch, repeating this process for epochs times
        generator = genSentences(datasetPath)
        w2v_model.train(generator, total_examples=w2v_model.corpus_count, epochs=1, callbacks=[bs])

        if callbacks:
            # saves the embeddings
            w2v_model.wv.save_word2vec_format(temporaryPath)
            # removes non-sense embeddings
            getSenseEmbeddings(temporaryPath, outputPath)

    if not callbacks:
        w2v_model.wv.save_word2vec_format(temporaryPath)
        getSenseEmbeddings(temporaryPath, outputPath)
    
    # deletes the formerly saved embeddings
    os.remove(temporaryPath)

    acc = wordSimilarityTask(wstPath, outputPath)

    if wstPath != "":
        bs.acc_wst_history.append(acc)

    if callbacks:
        return bs.acc_wst_history
    else:
        return acc

if __name__ == "__main__":
    # Code used to generate and train the model

    dev_path = "resources/developmentDataset.txt"
    train_path = "resources/trainingDataset.txt"
    embeddings_path = "resources/embeddings.vec"
    wst_path = "resources/combined.csv"

    SEED = 10
    EPOCHS = 3

    # Hyperparameters to test
    windows = [3, 5, 8]
    sizes = [50, 100, 300]
    samples = [1e-4, 1e-3]
    min_counts = [5, 7, 10]

    best_acc = 0
    best_window = 3
    best_size = 50
    best_sample = 6e-5
    best_min_count = 5
    best_alpha = 0.025
    best_min_alpha = 1e-4

    # Code which actually performed the gridSearch over the specified hyper parameters
    """ left = len(windows)*len(sizes)*len(samples)*len(min_counts)

    # performs gridSearch
    for window in windows:
        for size in sizes:
            for sample in samples:
                for min_count in min_counts:
                    res = train(dev_path, embeddings_path, wstPath=wst_path, min_count=min_count, window=window, size=size, sample=sample,
                                epochs=EPOCHS, seed=SEED, callbacks=True)
                    print("Window: {}, Size: {}, Sample: {}, Min_count: {}, Accuracies: {}".
                        format(window, size, sample, min_count, res))
                    left -= 1
                    print("Combinations left: {}".format(left))
                    if max(res) > best_acc:
                        best_acc = max(res)
                        best_window = window
                        best_size = size
                        best_sample = sample
                        best_min_count """
    
    # I've chosen these combinations and further investigated them
    combs = [[3, 50, 1e-4, 7], [3, 50, 1e-3, 7], [3, 100, 1e-3, 7], [3, 100, 1e-4, 7], [3, 300, 1e-4, 7],
             [3, 300, 1e-3, 7], [5, 50, 1e-3, 7], [5, 100, 1e-3, 7], [5, 300, 1e-4, 7], [8, 50, 1e-3, 7],
             [8, 50, 1e-4, 7], [8, 100, 1e-3, 7], [8, 100, 1e-3, 7], [8, 300, 1e-4, 7], [8, 100, 1e-3, 7]]

    left = len(combs)
    for window, size, sample, min_count in combs:
        res = train(dev_path, embeddings_path, wstPath=wst_path, min_count=min_count, window=window, size=size, sample=sample,
                    epochs=EPOCHS+2, seed=SEED, callbacks=True)
        print("Window: {}, Size: {}, Sample: {}, Min_count: {}, Accuracies: {}".
            format(window, size, sample, min_count, res))
        left -= 1
        print("Combinations left: {}".format(left))
        if max(res) > best_acc:
            best_acc = max(res)
            best_window = window
            best_size = size
            best_sample = sample
            best_min_count

    # Best accuracy up to this point: 0.221 params: size 300 window 8 min_count 7 sample 1e-3 alpha 0.025 min_alpha 1e-4

    alphas = [[0.05, 1e-4], [0.01, 5e-5]]

    left = len(alphas)
    for alpha, min_alpha in alphas:
        res = train(dev_path, embeddings_path, wstPath=wst_path, alpha=alpha, min_alpha=min_alpha,
                    min_count=best_min_count, window=best_window, size=best_size, sample=best_sample, epochs=EPOCHS, seed=SEED, callbacks=True)
        print("Alpha: {}, Min_alpha: {}, Accuracies: {}".format(alpha, min_alpha, res))
        left -= 1
        print("Combinations left: {}".format(left))
        if max(res) > best_acc:
            # during the first run best_alpha and best_min_alpha will be set to the default values of the model
            best_acc = max(res)
            best_alpha = alpha
            best_min_alpha = min_alpha

    # Best accuracy up to this point: 0.246 params: size 300 window 8 min_count 7 sample 1e-3 alpha 0.05 min_alpha 5e-4
    # There is the possibility that having higher alpha and min alpha values makes it perform better on few epochs and worse on more epochs
    # I don't have the right amount of time and computational power to train on many epochs so I'm blindly picking the higher values

    # Actual training
    res = train(train_path, embeddings_path, wstPath=wst_path, min_count=best_min_count, window=best_window, size=best_size,
                sample=best_sample, alpha=best_alpha, min_alpha=best_min_alpha, epochs=3, seed=SEED, callbacks=True)

    # Printing the results
    print("Final training accuracy history: {}".format(res))
    print("Best accuracy achieved: {}".format(max(res)))
    print("This accuracy has been achieved training for 10 epochs over the training dataset with the following parameters:\n"
          "Window: {}\n"
          "Size: {}\n"
          "Sample: {}\n"
          "Min_count: {}\n"
          "Alpha: {}\n"
          "Min_alpha: {}".
          format(best_window, best_size, best_sample, best_min_count, best_alpha, best_min_alpha))
