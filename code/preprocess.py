import tarfile
import os
import nltk
import sys
import string
import time

from html import unescape
from lxml import etree
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords

def findOverlappingIndices(start, stop, voc, keys):
    """
        This function looks to the keys of the dictionary voc which define an interval [key[0], key[1]) if any key is found such that this interval
        intersects with [start, stop) and such that it is not contained in keys, then such key[0], key[1] are returned, otherwise -1, -1 is returned
    """
    for key in voc:
        if key not in keys and not (start >= key[1] or stop <= key[0]):
            return key[0], key[1] 
    return -1, -1 # no overlapping indices pair has been found

def getStartAndStop(list, sublist):
    """
        returns either start and stop of the first occurrence of sublist in list or -1, -1 if sublist is not contained in list
    """

    for i in range(len(list)-len(sublist)+1):
        if list[i:i+len(sublist)] == sublist:
            return i, i+len(sublist)

    return -1, -1

def splitDatasets(path1, path2, dev_set, train_set):
    """
        Returns the dev_set as the 100.000 sentences of the path1 file concatenated with the first 200.000 of the path2 file
        and the train_set as the combination of the remainder of the 2.
    """
    fd_dev = open(dev_set, mode="w")
    fd_train = open(train_set, mode="w")
    f1 = open(path1, mode="r")
    f2 = open(path2, mode="r")

    i = 0
    line = f1.readline()
    while line != "":
        if i < 100000:
            fd_dev.write(line)
            i += 1
        else:
            fd_train.write(line)

        line = f1.readline()

    f1.close()

    line = f2.readline()
    while line != "":
        if i < 300000:
            fd_dev.write(line)
            i += 1
        else:
            fd_train.write(line)
            
        line = f2.readline()

    f2.close()
    fd_dev.close()
    fd_train.close()

def getMapping(tsvFile):
    """
        This function takes as input the path to a tsv file and returns the dictionary which maps the first element to the second one of every row of the file.
    """

    mappingFile = open(tsvFile, mode="r")
    mapping = dict()

    line = mappingFile.readline()

    while line != "":
        arr = line.split("\t")

        # mapping from babelNet to WordNet synset
        # notice: this discards wordnet offsets mapped to the same babelnet synset
        # it's done purposefully since it saves time when parsing the dataset
        # it shouldn't impact significantly the dataset since in the whole mapping file there are only 6 cases of bn synsets mapped to multiple wn offsets
        # the conditional assignment is needed to remove the "\n" character from the elements which contain it.
        mapping[arr[0]] = arr[1][:-1] if len(arr[1]) == 10 else arr[1]

        line = mappingFile.readline()

    return mapping

def parseEurosense(pathToXMLFile, pathToOutputFile, pathToMapping, verbose=False, progress=False, progress_step=100000):
    """
        This function parses the High Precision version of the Eurosense dataset using the method iterparse of the module lxml.etree
        Useful reference: https://www.ibm.com/developerworks/xml/library/x-hiperfparse/
        The Eurosense High Precision dataset is huge: 22.69gb as I'm writing and it wouldn't be possible to store it entirely on most of today's computers' memory.
        This is the reason why it is a better choice to use a generator rather than to read the whole file.

        Every line of the output file is composed of the english sentence where the consistent annotated anchors have been replaced by their senses.
        One anchor is considered being "consistent" iff:
            -The synset is in mapping, hence it has one corresponding offset in wordnet
            -The synset is coherent with the lemma according to wordnet
            -The anchor actually appears in the text

        The lines of the output file differ from the articles also because stop-words and punctuation(if tokenized) are removed, 
        and because every character is written in lowercase.

        The inputs are supposed to be the path to the XML file to parse, the path to the output file where the parsed version of the file will be written
        and the path to the mapping.
        NOTICE: if the path to the output file points to an already existing file it will be overwritten.
        There are also 3 optional parameters:
            -verbose which determines whether or not to print a final summary
                If unspecified or False, nothing will be printed.
            -progress which determines whether or not to print the number of parsed sentences after a progress_step
                If unspecified or False, nothing will be printed.
            -progress_step which, if progress is set to True, determines how often to print the number of parsed sentences
                If unspecified and progress=True, it will print the number of parsed sentences every 100.000 cases

        No return value provided.
    """

    # counters
    total_anchors = 0
    written_anchors = 0
    discarded_anchors_not_in_sentence = 0
    discarded_anchors_not_in_wordnet = 0
    discarded_anchors_not_coherent_with_wordnet = 0
    discarded_anchors_overlapping_indices = 0

    if progress:
        num_sentences = 0

    # iterates over the whole XML file
    iterator = etree.iterparse(pathToXMLFile, tag="sentence", events=("end", ))

    # used to remove the stop_words from the articles
    stop_words = stopwords.words("english")

    # used to check whether there exists a wn offset corresponding to the provided bn synset.
    mapping = getMapping(pathToMapping)

    file = open(pathToOutputFile, mode="w")

    for event, element in iterator:
        num_sentences += 1
        if progress and num_sentences % progress_step == 0:
            print("Parsing sentence number ", num_sentences)

        # new sentence tag
        for child in element:

            if child.tag == "text":
                # english sentences
                if child.attrib["lang"] == "en":

                    # list of the tokens in the sentence
                    sentence_tokenized = str(child.text).split()

                    # will store the anchors
                    anchors = dict()

            elif child.tag == "annotations":
                for elem in child:
                    # new annotation tag

                    # I gather one annotation if and only if, it:
                    #   -is labelled with attribute lang = "en"
                    #   -the specified anchor is actually contained in the sentence
                    #   -the babelnet synset maps to at least one wordnet offset
                    #   -the lemma is coherent with the synset
                    if elem.attrib["lang"] == "en":
                        total_anchors += 1

                        if elem.text not in mapping:
                            # if the synset is not in wordnet I don't need to save it
                            discarded_anchors_not_in_wordnet += 1
                            continue
                        if wn.of2ss(mapping[elem.text]) not in wn.synsets(elem.attrib["lemma"]):
                            # if the synset is not consistent with the lemma according to babelnet
                            discarded_anchors_not_coherent_with_wordnet += 1
                            continue
                        start, stop = getStartAndStop(sentence_tokenized, elem.attrib["anchor"].split())
                        if start == -1:
                            # if the anchor is not found I don't need to save it 
                            discarded_anchors_not_in_sentence += 1
                            continue
                        
                        # since it might happen that multiple pairs of indices generate an interval overlapping with [start, stop)
                        # I first save all the pair and if no one of these results being longer than stop - start I, only then, remove all of them
                        # removing a pair immediately might not be optimal since it's not given that the new anchor will be written
                        pairs = list()
                        old_start, old_stop = findOverlappingIndices(start, stop, anchors, pairs)
                        while old_start != -1:
                            if old_stop - old_start > stop - start:

                                # found one pair of indices which corresponds to a longer anchor.
                                # the readme of high-precision version of the Eurosense dataset says to keep the longest so this can be discarded
                                discarded_anchors_overlapping_indices += 1
                                break
                            
                            pairs.append((old_start, old_stop))
                            old_start, old_stop = findOverlappingIndices(start, stop, anchors, pairs)

                        if old_start != -1:
                            # it means that the break statement in the previous while has been encountered
                            continue
                        
                        for pair in pairs:
                            discarded_anchors_overlapping_indices += 1
                            del anchors[pair]

                        # creates the sense joining words with "_"
                        # e.g. mention = "come on" -> sense = come_on_synset
                        sense = "_".join(elem.attrib["lemma"].split()) + "_" + str(elem.text)
                        anchors[(start, stop)] = sense

                if anchors:

                    written_anchors += len(anchors)
                    for key in anchors:
                        if key[1] - key[0] != 1:
                            # adds a placeholder "." in order to keep the consistency of the following indices
                            # the placeholders will be removed when removing the punctuation
                            sentence_tokenized[key[0]+1:key[1]] = "."*(key[1]-key[0]-1)

                        # replaces the anchor with its sense
                        sentence_tokenized[key[0]] = anchors[key]
                        
                    
                    # outputs to the file the original sentence with lowercase characters, no stop-words, no punctuation 
                    # and the consistent anchors replaced with their senses. Actually only the longest anchor if many overlapping are found
                    file.write(" ".join(token.lower() 
                                for token in sentence_tokenized 
                                if token.lower() not in stop_words and any(c not in string.punctuation for c in token)) + "\n")                                  

            else:
                print("Found unexpected tag: " + child.tag)

        # These instructions are needed because even using the iterator the parsing would form a huge tree
        # which would be, once again, impossible, for most machines to store
        # It's safe to call clear() here because no descendant will be accessed
        element.clear()

        # also eliminate now-empty references from the root node to <Title>
        while element.getprevious() is not None:
            del element.getparent()[0]

    del iterator

    file.close()
    
    # summary
    if verbose:
        print("Number of parsed anchors: ", total_anchors)
        print("Number of written anchors: ", written_anchors)
        print("Number of anchors discarded because not in the sentence: ", discarded_anchors_not_in_sentence)
        print("Number of anchors discarded because the synset was not in WordNet: ", discarded_anchors_not_in_wordnet)
        print("Number of anchors discarded because the synset was not coherent with the lemma: ", discarded_anchors_not_coherent_with_wordnet)
        print("Number of anchors discarded because of overlapping indices: ", discarded_anchors_overlapping_indices)

def parseSEW(pathToTarFile, pathToOutputFile, pathToMapping, numArticles=-1, verbose=False, progress=False, progress_step=100000):
    """
        This function parses the Semantic Enriched Wikipedia conservative dataset without having to store and parse its unzipped version.
        The SEW Conservative dataset is composed of 721 folders each containing many xml files describing a wikipedia article each.

        Every line of the output file is composed of the text of one article of the dataset where the consistent annotated mentions have been replaced by their senses.
        One mention is considered being "consistent" iff:
            -The synset is in mapping, hence it has one corresponding offset in wordnet
            -The synset is coherent with the lemma according to wordnet
            -The mention actually appears in the text
        Also I will skip sentences with un-parsable characters which run into XMLSyntaxError. e.g. Emojis

        The lines of the output file differ from the articles also because stop-words and punctuation(if tokenized) are removed,
        and because every character is written in lowercase.

        The inputs are supposed to be the path to the tar file to parse, the path to the output file where the parsed version of the dataset will be written
        and the path to the mapping file.
        NOTICE: if the path to the output file points to an already existing file it will be overwritten.
        There are also 4 optional inputs: 
            - numArticles which must be an integer and determines the maximum number of articles we want to parse
                If unspecified or negative the whole dataset will be parsed.
            - verbose which determines whether to print or not a final summary
                If unspecified or False, nothing will be printed.
            - progress which determines whether to print information about the number of parsed articles
                If unspecified or False, nothing will be printed.
            - progress_step, if progress is set to True, progress_step will determine how many articles it will parse before printing the information
                If unspecified it will use a 100.000 step
            Notice: The progress information will be printed iff numArticles is set to some greater than zero value.


        No return value provided.
    """

    # counters
    total_mentions = 0
    written_mentions = 0
    discarded_mentions_not_in_sentence = 0
    discarded_mentions_not_in_wordnet = 0
    discarded_mentions_not_coherent_with_wordnet = 0

    # used to remove the stop_words from the articles
    stop_words = stopwords.words("english")

    # used to check whether there exists a wn offset corresponding to the provided bn synset.
    mapping = getMapping(pathToMapping)

    file = open(pathToOutputFile, mode="w")
    archive = tarfile.open(pathToTarFile)

    # Gathers one file from the archive
    xml_file = archive.next()

    # Needed to print information about the left articles 
    if progress:
        epoch = time.time()
        tot = numArticles

    while xml_file != None and numArticles != 0:

        if xml_file.name[-4:] == ".xml":
            # it actually extracts the file
            archive.extract(xml_file)

            # iterates over the just extracted file
            iterator = etree.iterparse(xml_file.name, tag="wikiArticle", events=("end", ))

            # this try clause is needed because some articles contain particular unescaped characters(tipically emojis) which make the iterator run into XMLParseError
            # When I run into such errors I just skip the article
            try:
                for event, element in iterator:

                    if element.attrib["language"] == "EN":
                        # Only english articles
                        for child in element:

                            if child.tag == "text":
                                # List of the tokens in the unescaped sentence
                                sentence_tokenized = unescape(str(child.text)).split()

                                # Dictionary which will contain all the consistent mentions
                                mentions = dict()

                            elif child.tag == "annotations":
                                for elem in child:
                                    # one annotation tag
                                    total_mentions += 1
                                    for el in elem:
                                        # tag inside one annotation tag
                                        if el.tag == "babelNetID":
                                            if el.text in mapping:
                                                synset = el.text
                                            else:
                                                # If the synset is not in the mapping it doesn't have a WN counterpart, hence I don't need this annotation
                                                discarded_mentions_not_in_wordnet += 1
                                                break

                                        elif el.tag == "mention":
                                            mention_tokenized = unescape(el.text).split()
                                            start, stop = getStartAndStop(sentence_tokenized, mention_tokenized)

                                            if start == -1:
                                                # Mention not found in the article
                                                discarded_mentions_not_in_sentence += 1
                                                break

                                            if wn.of2ss(mapping[synset]) not in wn.synsets(unescape(el.text)):
                                                # Synset is not consistent with the lemma
                                                discarded_mentions_not_coherent_with_wordnet += 1
                                                break

                                            # creates the sense joining words with "_"
                                            # e.g. mention = "come on" -> sense = come_on_synset
                                            sense = "_".join(mention_tokenized) + "_" + synset

                                            # saving the mention since it will be written
                                            mentions[(start, stop)] = sense

                                            written_mentions += 1

                                # If there is no mention in mentions it means that no consistently annotated mention has been found => the whole sentence will be discarded
                                if mentions:
                                    for key in mentions:
                                        if key[1] - key[0] != 1:
                                            # adds a placeholder "." in order to keep the consistency of the following indices
                                            # the placeholders will be removed when removing the punctuation
                                            sentence_tokenized[key[0]+1:key[1]] = "."*(key[1]-key[0]-1)
                                        # replaces the mention with its sense
                                        sentence_tokenized[key[0]] = mentions[key]

                                    # I remove both punctuation and stop words and write the resulting sentence to the output file
                                    file.write(" ".join(token.lower() 
                                                for token in sentence_tokenized 
                                                if token.lower() not in stop_words and any(c not in string.punctuation for c in token)) + "\n")
                # It's safe to call clear() here because no descendant will be accessed
                element.clear()

                # Also eliminate now-empty references from the root node to <Title>
                while element.getprevious() is not None:
                    del element.getparent()[0]
            
            except Exception as e:
                        # This exception is thought to capture the XMLSyntaxError which occurs in very few articles.
                        # The following condition is needed to avoid suppressing all the possible exceptions.
                        if type(e).__name__ != "XMLSyntaxError":
                            sys.exit("Error! Code: {}, Message: {}, File: {}".format(type(e).__name__, str(e), xml_file.name))

            del iterator

            # Deleting the file just parsed
            os.remove(xml_file.name)

            # Number of articles to parse
            numArticles -= 1

            if progress and numArticles % progress_step == 0 and numArticles > 0:
                # Works as a progress bar
                elapsed = time.time() - epoch
                left = int(elapsed/60 * numArticles / (tot - numArticles))
                print("Articles left to parse: {}, time elapsed: {} minutes, estimated time left: {} minutes".format(
                    numArticles, "%.2f" % round(elapsed/60, 2), left if left > 1 else "<1"))

        # next article to parse
        xml_file = archive.next()

    # closing file descriptors
    file.close()
    archive.close()

    # summary
    if verbose:
        print("Number of parsed mentions: ", total_mentions) 
        print("Number of written mentions: ", written_mentions)
        print("Number of mentions discarded because not in the sentence: ", discarded_mentions_not_in_sentence)
        print("Number of mentions discarded because the synset was not in WordNet: ", discarded_mentions_not_in_wordnet)
        print("Number of mentions discarded because the synset was not coherent with the lemma: ", discarded_mentions_not_coherent_with_wordnet)

if __name__ == "__main__":

    # Downloads resources needed for coherence check and stopwords removal.
    nltk.download("wordnet", quiet=True)
    nltk.download("stopwords", quiet=True)
    
    # Code used to generate the parsed corpora.
    # Not the datasets nor their parsed version have been uploaded, as specified.
    parseEurosense("path_to_eurosense.xml",
                   "resources/parsedEurosense.txt",
                   "resources/bn2wn_mapping.txt",
                   verbose=True,
                   progress=True,
                   progress_step=100000)

    parseSEW("path_to_sew.tar.gz",
             "resources/parsedSEW.txt",
             "resources/bn2wn_mapping.txt",
              numArticles=2000000, 
              verbose=True, 
              progress=True, 
              progress_step=100000)

    splitDatasets("resources/parsedEurosense.txt",
                  "resources/parsedSEW.txt",
                  "resources/developmentDataset.txt",
                  "resources/trainingDataset.txt")
