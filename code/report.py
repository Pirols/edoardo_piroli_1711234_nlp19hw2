# This file contains the code I've used to gather information and plots for the report

import nltk
import sklearn
import matplotlib.pyplot as plt

from gensim.models import KeyedVectors
from wordcloud import WordCloud
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
from sklearn.manifold import TSNE

from preprocess import getMapping

def getSensesText(dataset_path, stopwords):
    """
        This function returns the text of the file in dataset_path excluding non-senses, senses with lemmas in stopwords and non-polysemous senses
    """
    fd = open(dataset_path, mode="r")
    
    line = fd.readline()
    
    text = ""
    
    while line != "":
        words = line.split()
        
        for word in words:
            if "_bn:" in word and word[:word.rfind("_")] not in stopwords and len(wn.synsets(word[:word.rfind("_")])) > 1:
                text += word + " "
                
        line = fd.readline()
        
    fd.close()
        
    return text

def getWordCloud(text, outputPath="./wordcloud.png", max_words=200, width=400, height=200):
    """
        This function generates the wordcloud plot of my training senses and saves i in outputPath
    """
    wordcloud = WordCloud(max_words=max_words, width=width, height=height, background_color="white").generate(text)
    wordcloud.to_file(outputPath)

def getAccuracyPlot(outputPath):
    """
        This function plots the accuracy evolution of my model and saves it in outputPath
    """
    plt.plot([1, 2, 3], [0.43385105163806903, 0.4815691277955578, 0.48699567697717827])
    plt.xticks(range(1,4))
    plt.yticks([0.425, 0.45, 0.475, 0.5])
    plt.ylabel("Accuracy")
    plt.xlabel("Epochs")
    plt.savefig(outputPath, bbox_inches='tight')

def getTSNEPlot(outputPath, embeddings, labels, width=30, height=18):
    """
        This function plots one tsne representation and saves it in outputPath
    """
    tsne = TSNE(perplexity=30, n_components=2, init="pca")

    low_dim_embds = tsne.fit_transform(embeddings)

    plt.figure(figsize=(width, height)) # inches
    for i, label in enumerate(labels):
        x, y = low_dim_embds[i, :]
        plt.scatter(x, y)
        plt.annotate(label, xy=(x, y), xytext=(5, 2), textcoords="offset points", ha="right", va="bottom")

    plt.savefig(outputPath)

if __name__ == "__main__":
    # This code generates all the plots and some information I've used in the report.

    # Downloads resources needed to get the meaning of senses and to remove senses with lemma in stop words
    nltk.download("wordnet", quiet=True)
    nltk.download("stopwords", quiet=True)

    stop_words = stopwords.words("english")

    train_path = "resources/trainingDataset.txt"
    mapping_path = "resources/bn2wn_mapping.txt"
    embeddings_path = "resources/embeddings.vec"

    # only senses will be in text
    text = getSensesText(train_path, stop_words)
    # plots the wordcloud plot of the senses in the training path
    getWordCloud(text, outputPath="resources/wordcloud.png", max_words=200, width=480, height=320)
    # plots the accuracy evolution
    getAccuracyPlot("resources/accuracy.png")

    # sets up for the tsne plot
    embeddings = list()
    labels = list()

    tsne_plotted_senses = 100

    with open(embeddings_path, mode="r") as fd:
        i = 0
        fd.readline()

        line = fd.readline()

        while i < tsne_plotted_senses:
            words = line.split()
            labels.append(words[0])
            embeddings.append(words[1:])

            line = fd.readline()
            i += 1

    # plots the tsne plot of the first 100 embeddings
    getTSNEPlot("resources/tsne.png", embeddings, labels)

    # printing the 3 most similar senses of some other senses
    w2v_model = KeyedVectors.load_word2vec_format(embeddings_path, binary=False)

    mapping = getMapping(mapping_path)

    very_popular = ["located_bn:00106113a", "parliament_bn:00060708n", "france_bn:00036202n", "village_bn:00042729n"]
    not_popular = ["russia_bn:00068622n", "history_bn:00044268n", "municipality_bn:00056337n", "industry_bn:00046576n"]

    print("Working on really popular senses:")
    for sense in very_popular:
        bn_synset = sense[sense.rfind("_")+1:]
        wn_synset = wn.of2ss(mapping[bn_synset])
        print("Sense: {}, 5 most similar senses: {}".format(sense, w2v_model.most_similar(sense, topn=3)))

    print("Working on not popular senses:")
    for sense in not_popular:
        bn_synset = sense[sense.rfind("_")+1:]
        wn_synset = wn.of2ss(mapping[bn_synset])
        print("Sense: {}, 5 most similar senses: {}".format(sense, w2v_model.most_similar(sense, topn=3)))